package com.example.kotlinsharedpreferences

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    //ref to our interface and preference manager class
    //here we lazy load our PreferenceManager class : will only be loaded in our runtime when its operation is met
    // the first call to get() executes the lambda passed to lazy() and remembers the result, subsequent calls to get() simply return the remembered result
    private val preferenceHelper: IPreferenceHelper by lazy {PreferenceManager(applicationContext)}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //displaying saved data
        textView.text = " API KEY -> ${preferenceHelper.getApiKey()} \n User id -> ${preferenceHelper.getUserId()} "
        //saving data and updating textview when button is clicked
        button.setOnClickListener {
            //update data in shared preference
            preferenceHelper.setApiKey(editText.text.toString())
            preferenceHelper.setUserId(editText2.text.toString())
            //display data
            textView.text = " API KEY -> ${preferenceHelper.getApiKey()} \n User id -> ${preferenceHelper.getUserId()} "

        }
    }
}