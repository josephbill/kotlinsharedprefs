package com.example.kotlinsharedpreferences

import android.content.Context
import android.content.SharedPreferences
//making this class open allows it to be inherited
//we will also make this class implement our Ipreference helper
//so as to gain access to our abstract method for set/get : APIKEY and USERID
open class PreferenceManager constructor(context: Context) : IPreferenceHelper {
    //private constant variable that can access the shared preferences file
    private val PREFS_NAME = "SharedPreferencesDemo"
    private var preferences: SharedPreferences
    //The code inside the init block is the first to be executed when the class is instantiated.
    //basically our sharedpreferences will be set to a mode before they can be used or accessed
    //MODE_PRIVATE : he default mode, where the created file can only be accessed by the calling application (or all applications sharing the same user ID)
    init {
        //declaring access level of our shared preferences
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }


    //using our methods to set values in our preferences
    override fun setApiKey(apiKey: String) {
        preferences[API_KEY] = apiKey
    }

    override fun getApiKey(): String {
        return preferences[API_KEY] ?: ""
    }

    override fun setUserId(userId: String) {
        preferences[USER_ID] = userId
    }

    override fun getUserId(): String {
         return  preferences[USER_ID] ?: ""
    }

    //method for clearing our data set in the preferences
    override fun clearPrefs() {
        preferences.edit().clear().apply()
    }

    //declaring a companion object
    // companion objects are singleton objects whose properties
    // and functions are tied to a class but not to the instance of that class
    //our object will have the values we want to save in our preferences
    companion object {
        const val API_KEY = "api_key"
        const val USER_ID = "user_id"
    }

    //the writing process
    /**
     * SharedPreferences extension function, to listen the edit() and apply() fun calls
     * on every SharedPreferences operation.
     */
    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = this.edit()
        operation(editor)
        editor.apply()
    }

    /**
     * puts a key value pair in shared prefs if doesn't exists, otherwise updates value on given [key]
     */
    private operator fun SharedPreferences.set(key: String, value: Any?) {
        when (value) {
            is String? -> edit { it.putString(key, value) }
            is Int -> edit { it.putInt(key, value) }
            is Boolean -> edit { it.putBoolean(key, value) }
            is Float -> edit { it.putFloat(key, value) }
            is Long -> edit { it.putLong(key, value) }
            else -> throw UnsupportedOperationException("Not yet implemented")
        }
    }

    //the reading process
    /**
     * finds value on given key.
     * [T] is the type of value
     * @param defaultValue optional default value - will take null for strings, false for bool and -1 for numeric values if [defaultValue] is not specified
     */
    private inline operator fun <reified T : Any> SharedPreferences.get(
        key: String,
        defaultValue: T? = null
    ): T? {
        return when (T::class) {
            String::class -> getString(key, defaultValue as? String) as T?
            Int::class -> getInt(key, defaultValue as? Int ?: -1) as T?
            Boolean::class -> getBoolean(key, defaultValue as? Boolean ?: false) as T?
            Float::class -> getFloat(key, defaultValue as? Float ?: -1f) as T?
            Long::class -> getLong(key, defaultValue as? Long ?: -1) as T?
            else -> throw UnsupportedOperationException("Not yet implemented")
        }
    }




}
