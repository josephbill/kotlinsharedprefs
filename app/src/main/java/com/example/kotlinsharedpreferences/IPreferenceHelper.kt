package com.example.kotlinsharedpreferences

interface IPreferenceHelper {
    //interface will define get and setters for storing and retrieving values
    //in this example we will be storing an API key for x service and UserID of the
    //current person using the app

    fun setApiKey(apiKey: String)
    fun getApiKey() : String
    fun setUserId(userId: String)
    fun getUserId() : String
    fun clearPrefs()
}

